using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class Launcher : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject controlPanel;

    [SerializeField]
    private Text feedbackText;

    [SerializeField]
    private byte maxPlayersPerRoom = 2;

    bool isConnecting;

    string gameVersion = "1";

    [Space(10)]
    [Header("Custom Variables")]
    public InputField playerNameField;
    public InputField roomNameField;

    [Space(5)]
    public Text playerStatus;
    public Text connectionStatus;

    [Space(5)]
    public GameObject roomJoinUI;
    public GameObject buttonLoadArena;
    public GameObject buttonJoinRoom;

    string playerName = "";
    string roomName = "";

    // Start Method
    void Start()
    {
        PlayerPrefs.DeleteAll();
        roomJoinUI.SetActive(false);
        buttonLoadArena.SetActive(false);
        ConnectPhoton();
        
    }
    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }
    // Helper Method
    public void ConnectPhoton()
    {
        connectionStatus.text = "Connecting...";
        PhotonNetwork.GameVersion = gameVersion;
        PhotonNetwork.ConnectUsingSettings();
    }

    public void JoinRoom()
    {
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.LocalPlayer.NickName = playerName;
            Debug.Log("Photon isconnected | Join/Create Room"+roomNameField.text);
            RoomOptions roomOptions = new RoomOptions();
            TypedLobby typedLobby = new TypedLobby(roomName, LobbyType.Default);
            PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, typedLobby);
        }
    }
    public void LoadArena()
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount > 1)
            PhotonNetwork.LoadLevel("MainArena");
        else
            playerStatus.text = "Minimum 2 players required to Load Arena";
    }
    public void SetPlayerName(string name)
    {
        playerName = name;
    }
    public void SetRoomName(string name)
    {
        roomName = name;
    }
    // Update Method

    //Photon Method
    public override void OnConnected()
    {
        connectionStatus.text = "Connected to Server";
        connectionStatus.color = Color.green;
        roomJoinUI.SetActive(true);
        buttonLoadArena.SetActive(false);
    }
    public override void OnJoinedRoom()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            playerStatus.text = "You are Room leader! Load Arena";
            buttonJoinRoom.SetActive(false);
            buttonLoadArena.SetActive(true);
        }
        else
        {
            playerStatus.text = "You join room";
        }
    }
    public override void OnDisconnected(DisconnectCause cause)
    {
        isConnecting = false;
        controlPanel.SetActive(true);
        Debug.LogError("Disconnected");
    }
}
